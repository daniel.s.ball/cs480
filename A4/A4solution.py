import neuralnetworks as nn
import mlutils as ml
import numpy as np

def trainLDA(X,T,parameters):
    pass

def evaluateLDA(model,X,T):
    pass

def trainNN(X,T,parameters):
    pass

def evaluateNN(model,X,T):
    pass

resultsLDA = ml.trainValidateTestKFoldsClassification( trainLDA,evaluateLDA, X,Tclasses, [None], 
                                                      nFolds=6, shuffle=False,verbose=False)
printResults('LDA:',resultsLDA)

resultsNN = ml.trainValidateTestKFoldsClassification( trainNN,evaluateNN, X,Tclasses, 
                                                     [ [ [0], 10], [[10], 100] ],
                                                     nFolds=6, shuffle=False,verbose=False)
printResults('NN:',resultsNN)